$(function() {
    $('.form-item-password-button').click(function(e) {
		e.preventDefault();
		var passworInput = $(this).parents('.form-item-password').find('.form-item-password-input'),
			type = passworInput.attr('type') == "text" ? "password" : 'text';
        passworInput.prop('type', type);
    });
});